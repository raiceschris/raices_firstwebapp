# Raices_FirstWebApp

This is my first web application utilizing an MVC design and a servlet as my controller.
The application accepts a plain text file and prints the contents on another page. It checks
if the file has a .txt extension otherwise an error message will be printed. It also should print
an image file, however, since the file is not moved to the webapp folder of the project the result is 
the broken image icon (I was unsure how to resolve this). Another issue is that the path name of the file is 
fixed. I used getAbsolutePath() on the File object but the path was always resolved to 
this directory:C:\Users\raice\Desktop\spring-tool-suite-4-4.0.2.RELEASE-e4.9.0-win32.win32.x86_64\sts-4.0.2.RELEASE
even though the file the user (in this case me) chose was in my desktop so the pathname should have been C:\Users\raice\Desktop 
 

