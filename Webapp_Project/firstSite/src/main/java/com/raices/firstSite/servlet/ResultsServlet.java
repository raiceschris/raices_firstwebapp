package com.raices.firstSite.servlet;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ResultsServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
		String fileFromPage = req.getParameter("fileUpload");
		File fileName = new File("C:\\Users\\raice\\Desktop\\" + fileFromPage);
		PrintWriter out = resp.getWriter();
		
		BufferedImage image=ImageIO.read(fileName);
		boolean isPlainTxt=false;
		
		if(fileFromPage.matches(".*txt"))
			isPlainTxt=true;
		else
			isPlainTxt=false;
		
		
		if(image==null)
		{
			if(isPlainTxt) {
				BufferedReader br = new BufferedReader(new FileReader(fileName));
				String buffer;
			
				while((buffer=br.readLine()) != null)
				{
					out.println(buffer);
				}
			
				br.close();
			}
			else {
				out.println("This document is not supported");
			}
		}
		else {
			out.println("<html>" +
		            "<head></head>" +
		            "<body>" +
		            "<h1>Your Image</h1><br>" +
		            "<img src=" + fileName + ">" +
		            "</body>" +
		         "</html>");
		}
		//File fileName = new File(System.getProperty("user.dir") + "\\" + fileFromPage);
		
		
		/*File fileName = new File(req.getParameter("fileUpload"));
		PrintWriter out = resp.getWriter();
		out.println(fileName.getCanonicalFile());*/
		
		
	}
	

}
